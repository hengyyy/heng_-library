/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 80029
 Source Host           : localhost:3306
 Source Schema         : books

 Target Server Type    : MySQL
 Target Server Version : 80029
 File Encoding         : 65001

 Date: 11/11/2022 17:00:48
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for t_book
-- ----------------------------
DROP TABLE IF EXISTS `t_book`;
CREATE TABLE `t_book`  (
  `id` int(0) NOT NULL AUTO_INCREMENT COMMENT '书籍id',
  `book_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '书籍名称',
  `author` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '作者',
  `publish` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '出版社',
  `isbn` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '书籍编码',
  `introduction` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '书籍介绍',
  `language` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '书籍语言',
  `price` decimal(10, 2) NULL DEFAULT NULL COMMENT '书籍售价',
  `pubdate` datetime(0) NULL DEFAULT NULL COMMENT '发布日期',
  `pressmark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '书架号',
  `state` int(0) NULL DEFAULT 0 COMMENT '书籍状态  0 空闲 1借阅 2下架 3 其他',
  `is_deleted` int(0) NULL DEFAULT 1 COMMENT '删除状态  0 删除 1未删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_book
-- ----------------------------
INSERT INTO `t_book` VALUES (1, '时间简史', '霍金', '清华出版社', 'xxxxx01012123232', '一本神奇的书籍...666666', '中文', 59.90, '1997-05-06 00:00:00', '1-3-03', 0, 1);
INSERT INTO `t_book` VALUES (6, '时间简史(插图版)', '霍金', '清华出版社', 'xxxxx0101212323', '时间只留简史，世界再无霍金', '中文', 59.90, '1997-05-05 00:00:00', '1-3-03', 0, 1);
INSERT INTO `t_book` VALUES (11, 'Java从入门到放弃', '恒牛科技', '加里敦大学出版社', '4664131486461', '                                        \r\n                ....\r\n                ', '中文', 39.90, '2022-10-26 00:00:00', '1-1-04', 0, 1);
INSERT INTO `t_book` VALUES (12, 'Java修炼指南', '开课吧组编', '机械工业出版社', '4664131486461216513131', '                    这是一本讲Java JDK源码的书\r\n                \r\n                ', 'selected', 59.90, '2020-08-08 16:00:00', '1-1-06', 0, 1);
INSERT INTO `t_book` VALUES (13, 'Java Web入门很轻松', '云尚科技', '清华大学出版社', '431354681513132131', 'java web是...                    \r\n                ', '中文', 55.86, '2022-02-28 00:00:00', '1-1-08', 0, 1);
INSERT INTO `t_book` VALUES (14, 'Java编程思维', '艾伦·唐尼（AllenBDowney）', '人民邮电出版社', '4916310031313123165', '                                        \r\n      ...          \r\n                ', '中文', 23.99, '2016-11-29 00:00:00', '1-1-10', 0, 1);

-- ----------------------------
-- Table structure for t_class
-- ----------------------------
DROP TABLE IF EXISTS `t_class`;
CREATE TABLE `t_class`  (
  `id` int(0) NOT NULL AUTO_INCREMENT COMMENT '班级编号',
  `class_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '班级名称',
  `class_desc` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '班级描述',
  `dept_id` int(0) NULL DEFAULT NULL COMMENT '所属系别编号',
  `dept_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '所属系别名称-冗余字段',
  `is_deleted` int(0) NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 34 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_class
-- ----------------------------
INSERT INTO `t_class` VALUES (1, '软件工程01班', '软件工程01班', 1, '计算机系', 1);
INSERT INTO `t_class` VALUES (2, '软件工程02班', '软件工程02班', 1, '计算机系', 1);
INSERT INTO `t_class` VALUES (4, '应用英语22级01班', '应用英语22级01班', 2, '英语系', 1);
INSERT INTO `t_class` VALUES (5, '应用英语22级02班', '应用英语22级02班', 19, '英语系', 1);
INSERT INTO `t_class` VALUES (6, '物理学21级01班', '物理学21级01班', 23, '物理系', 1);
INSERT INTO `t_class` VALUES (14, '土木工程22级02班', '土木工程........', 20, '土木工程系', 1);
INSERT INTO `t_class` VALUES (33, '广告学01班', '广告学......', 25, '广告学', 1);
INSERT INTO `t_class` VALUES (34, '广告学01班', '广告学......', 25, '广告学', 1);
INSERT INTO `t_class` VALUES (35, '英语系', '英语.......', 19, '英语系', 1);
INSERT INTO `t_class` VALUES (36, '英语系', '英语.......', 19, '英语系', 0);

-- ----------------------------
-- Table structure for t_department
-- ----------------------------
DROP TABLE IF EXISTS `t_department`;
CREATE TABLE `t_department`  (
  `id` int(0) NOT NULL AUTO_INCREMENT COMMENT '系别id',
  `department` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '系别名称',
  `dept_desc` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '系别描述',
  `is_deleted` int(0) UNSIGNED NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 25 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_department
-- ----------------------------
INSERT INTO `t_department` VALUES (15, '广告学', '广告学哈哈哈哈', 1);
INSERT INTO `t_department` VALUES (16, '土木工程系', '土木工程系......', 1);
INSERT INTO `t_department` VALUES (17, '计算机系555', '计算机学院1', 1);
INSERT INTO `t_department` VALUES (18, '广告学', '广告学......1', 1);
INSERT INTO `t_department` VALUES (19, '英语系', '英语系.....', 1);
INSERT INTO `t_department` VALUES (20, '土木工程系', '土木工程系......', 1);
INSERT INTO `t_department` VALUES (23, '物理系', '物理系......', 1);
INSERT INTO `t_department` VALUES (24, '化工系', '化工系...', 1);
INSERT INTO `t_department` VALUES (25, '广告学', '广告学......', 1);

-- ----------------------------
-- Table structure for t_lend_list
-- ----------------------------
DROP TABLE IF EXISTS `t_lend_list`;
CREATE TABLE `t_lend_list`  (
  `id` int(0) NOT NULL AUTO_INCREMENT COMMENT '借阅记录id',
  `book_id` int(0) NULL DEFAULT NULL COMMENT '书籍编号',
  `card_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '书籍卡编号',
  `lend_date` datetime(0) NULL DEFAULT NULL COMMENT '借阅时间',
  `back_date` datetime(0) NULL DEFAULT NULL COMMENT '归还时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 58 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_lend_list
-- ----------------------------
INSERT INTO `t_lend_list` VALUES (31, 2, '8', '2022-10-28 07:58:39', '2022-10-28 08:06:07');
INSERT INTO `t_lend_list` VALUES (38, 1, '11', '2022-10-28 08:19:28', '2022-11-04 01:15:29');
INSERT INTO `t_lend_list` VALUES (40, 1, '3', '2022-10-28 08:23:22', '2022-10-28 08:23:34');
INSERT INTO `t_lend_list` VALUES (43, 14, '10', '2022-11-04 01:33:12', '2022-11-04 01:33:28');
INSERT INTO `t_lend_list` VALUES (44, 1, '13', '2022-11-04 01:54:22', '2022-11-04 02:41:54');
INSERT INTO `t_lend_list` VALUES (47, 1, '18', '2022-11-04 02:24:19', '2022-11-04 02:25:01');
INSERT INTO `t_lend_list` VALUES (51, 1, '20', '2022-11-04 02:47:31', '2022-11-04 03:07:40');
INSERT INTO `t_lend_list` VALUES (53, 6, '22', '2022-11-04 03:07:35', '2022-11-04 03:08:36');
INSERT INTO `t_lend_list` VALUES (54, 1, '23', '2022-11-04 03:11:02', '2022-11-04 03:15:27');
INSERT INTO `t_lend_list` VALUES (55, 1, '24', '2022-11-04 03:16:30', '2022-11-04 03:18:25');
INSERT INTO `t_lend_list` VALUES (56, 12, '25', '2022-11-04 03:27:51', '2022-11-04 03:40:57');
INSERT INTO `t_lend_list` VALUES (57, 11, '26', '2022-11-04 03:45:47', '2022-11-11 08:19:52');
INSERT INTO `t_lend_list` VALUES (58, 1, '27', '2022-11-04 03:48:45', '2022-11-11 08:39:59');
INSERT INTO `t_lend_list` VALUES (59, 12, '26', '2022-11-11 08:19:40', NULL);
INSERT INTO `t_lend_list` VALUES (60, 1, '27', '2022-11-11 08:20:15', NULL);
INSERT INTO `t_lend_list` VALUES (61, 12, '27', '2022-11-11 08:39:52', NULL);

-- ----------------------------
-- Table structure for t_reader_card
-- ----------------------------
DROP TABLE IF EXISTS `t_reader_card`;
CREATE TABLE `t_reader_card`  (
  `id` int(0) NOT NULL AUTO_INCREMENT COMMENT '借书卡id',
  `stu_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '学生编号',
  `stu_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '学生姓名-冗余字段',
  `password` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '借书卡密码',
  `state` int(0) NULL DEFAULT 0 COMMENT '借书卡状态 0 未使用  1 已使用',
  `is_deleted` int(0) UNSIGNED NULL DEFAULT 1 COMMENT '是否被删除 0 删除 1未删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 27 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_reader_card
-- ----------------------------
INSERT INTO `t_reader_card` VALUES (26, '20', '李四', '2f8e8e06-ae2a-4296-9d78-93d1fa1e1387', 0, 1);
INSERT INTO `t_reader_card` VALUES (27, '21', '张三', 'ed692502-95df-4d8c-8006-e95634e317f3', 0, 1);

-- ----------------------------
-- Table structure for t_student
-- ----------------------------
DROP TABLE IF EXISTS `t_student`;
CREATE TABLE `t_student`  (
  `id` int(0) NOT NULL AUTO_INCREMENT COMMENT '学员id',
  `stu_num` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '学籍编号',
  `stu_name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '学员姓名',
  `phone_num` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '手机号',
  `gender` varchar(5) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '性别',
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '家庭地址',
  `classid` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '所属班级',
  `class_name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '班级名称-冗余字段',
  `departid` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '所属系别-冗余字段',
  `depart_name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '院系名称-冗余字段',
  `is_deleted` int(0) NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 21 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_student
-- ----------------------------
INSERT INTO `t_student` VALUES (20, '2022101002', '李四', '13158978456', '女', '云南昆明', '6', '物理学21级01班', '23', '物理系', 1);
INSERT INTO `t_student` VALUES (21, '2022101001', '张三', '15612437766', '男', '丽江', '6', '物理学21级01班', '23', '物理系', 1);

-- ----------------------------
-- Table structure for t_user
-- ----------------------------
DROP TABLE IF EXISTS `t_user`;
CREATE TABLE `t_user`  (
  `id` int(0) NOT NULL AUTO_INCREMENT COMMENT '用户id',
  `user_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '账号',
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '密码',
  `phone_num` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '手机号码',
  `email` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '邮箱',
  `is_deleted` int(0) UNSIGNED NULL DEFAULT 1 COMMENT '是否删除 0 删除 1未删除',
  `salt` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '加密-盐值',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 45 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_user
-- ----------------------------
INSERT INTO `t_user` VALUES (1, 'admin', '123', '13312344321', '279583842222@qq.com', 1, NULL);
INSERT INTO `t_user` VALUES (43, '张三', '123456', '15612437765', NULL, 0, NULL);
INSERT INTO `t_user` VALUES (44, '13158978456', '123456', '13158978456', NULL, 1, NULL);
INSERT INTO `t_user` VALUES (45, '15612437766', '123456', '15612437766', NULL, 1, NULL);

SET FOREIGN_KEY_CHECKS = 1;
