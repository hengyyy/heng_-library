package com.books.dao;


import com.books.bean.User;

import java.util.List;

/**
 * @author xiaolong
 * @date 2022年10月11日 15:37
 * 用户的持久层接口
 */

public interface UserDao {
    /**
     * 根据用户信息查询用户
     *
     * @param user
     * @return
     */
    public List<User> list(User user);

    /**
     * 保存用户信息
     *
     * @param user
     * @return
     */
    public Integer save(User user);

    /**
     * 根据id删除用户信息
     *
     * @param id
     * @return
     */
    public Integer deleteById(Integer id);

    /**
     * 根据id查询用户信息
     *
     * @param id
     * @return
     */
    public User queryById(Integer id);

    /**
     * 更新用户id
     *
     * @param user
     * @return
     */

    public Integer updateUser(User user);

    /**
     * 检查账号是否存在
     *
     * @param userName
     * @return
     */

    public String checkUserName(String userName);

    /**
     * 验证登录功能
     *
     * @param userName
     * @param password
     * @return User null 登录失败
     */
    public User checkUserNameAndPassword(String userName, String password);


}
