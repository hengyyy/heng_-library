package com.books.dao.impl;

import com.books.bean.Book;
import com.books.dao.BookDao;
import com.books.utils.DelFlagE;
import com.books.utils.MyDbutils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * @author xiaolong
 * @date 2022年10月18日 16:28
 * BookDao的实现
 */

public class BookDaoImpl implements BookDao {

    String sql;
    QueryRunner queryRunner;

    /**
     * 查询全部
     *
     * @param book
     * @return
     */
    @Override
    public List<Book> list(Book book) {
        queryRunner = MyDbutils.getQueryRunner();
        sql = "select * from t_book where is_deleted = ?";
        try {
            return queryRunner.query(sql, new ResultSetHandler<List<Book>>() {
                @Override
                public List<Book> handle(ResultSet rs) throws SQLException {
                    List<Book> list = new ArrayList<>();
                    while (rs.next()) {
                        Book b = new Book();
                        b.setId(rs.getInt("id"));
                        b.setBookName(rs.getString("book_name"));
                        b.setAuthor(rs.getString("author"));
                        b.setPublish(rs.getString("publish"));
                        b.setIsbn(rs.getString("isbn"));
                        String introduction = rs.getString("introduction");
                        if (introduction != null && !"".equals(introduction) && introduction.length() > 20) {
                            introduction = rs.getString("introduction").substring(0, 20);
                        }
                        b.setIntroduction(introduction + "...");
                        b.setLanguage(rs.getString("language"));
                        b.setPrice(rs.getBigDecimal("price"));
                        b.setPubdate(rs.getDate("pubdate"));
                        b.setPressmark(rs.getString("pressmark"));
                        b.setIsDeleted(rs.getInt("is_deleted"));
                        b.setState(rs.getInt("state"));
                        list.add(b);
                    }
                    return list;
                }
            }, DelFlagE.NO.code);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 更新书籍
     *
     * @param book
     * @return
     */
    @Override
    public Integer saveBook(Book book) {
        queryRunner = MyDbutils.getQueryRunner();
        sql = "insert into t_book(book_name,author,publish,isbn,introduction,language,price,pubdate,pressmark)values(?,?,?,?,?,?,?,?,?)";
        try {
            return queryRunner.update(sql,
                    book.getBookName(), book.getAuthor(), book.getPublish(),
                    book.getIsbn(), book.getIntroduction(), book.getLanguage(),
                    book.getPrice(), book.getPubdate(), book.getPressmark());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }

    /**
     * 测试
     *
     * @param args
     */
    public static void main(String[] args) {
        Book book = new Book();
        book.setId(1);
        book.setBookName("时间简史");
        book.setAuthor("霍金");
        book.setPrice(new BigDecimal(59.9));
        book.setLanguage("中文");
        Calendar calendar = Calendar.getInstance();
        calendar.set(1997, 4, 6);
        book.setPubdate(calendar.getTime());
        book.setIntroduction("一本神奇的书籍...666666");
        book.setIsbn("xxxxx01012123232");
        book.setPressmark("1-3-03");
        book.setPublish("清华出版社");
//        dao.saveBook(book);
        BookDaoImpl dao = new BookDaoImpl();
        dao.updateBook(book);
        List<Book> list = dao.list(null);
        System.out.println(list);
    }

    @Override
    public Integer updateBook(Book book) {
        queryRunner = MyDbutils.getQueryRunner();
        if (book.getIsDeleted() == null || "".equals(book.getIsDeleted())) {
            book.setIsDeleted(DelFlagE.NO.code);
        }
        if (book.getState() == null || "".equals(book.getState())) {
            book.setState(0);
        }
        sql = "update t_book set book_name =?,author=?,publish=?,isbn=?,introduction=?,language=?,price=?,pubdate=?,state=?,is_deleted=? where id=?";
        try {
            return queryRunner.update(sql,
                    book.getBookName(), book.getAuthor(),
                    book.getPublish(), book.getIsbn(),
                    book.getIntroduction(), book.getLanguage(),
                    book.getPrice(), book.getPubdate(),
                    book.getState(), book.getIsDeleted(), book.getId());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }

    @Override
    public Integer deleteById(Integer id) {
        QueryRunner queryRunner = MyDbutils.getQueryRunner();
        sql = "update t_book set is_deleted=? where id=?";
        try {
            return queryRunner.update(sql, DelFlagE.YES.code, id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }

    @Override
    public Book queryById(Integer id) {
        queryRunner = MyDbutils.getQueryRunner();
        sql = "select * from t_book where is_deleted = ? and id = ?";
        try {
            return queryRunner.query(sql, new ResultSetHandler<Book>() {
                @Override
                public Book handle(ResultSet rs) throws SQLException {
                    if (rs.next()) {
                        Book b = new Book();
                        b.setId(rs.getInt("id"));
                        b.setBookName(rs.getString("book_name"));
                        b.setAuthor(rs.getString("author"));
                        b.setPublish(rs.getString("publish"));
                        b.setIsbn(rs.getString("isbn"));
                        String introduction = rs.getString("introduction");
                        b.setIntroduction(introduction);
                        b.setLanguage(rs.getString("language"));
                        b.setPrice(rs.getBigDecimal("price"));
                        b.setPubdate(rs.getDate("pubdate"));
                        b.setPressmark(rs.getString("pressmark"));
                        b.setIsDeleted(rs.getInt("is_deleted"));
                        b.setState(rs.getInt("state"));
                        return b;
                    }
                    return null;
                }
            }, DelFlagE.NO.code, id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;

    }

    @Override
    public List<Book> queryListByState(int code) {
        queryRunner = MyDbutils.getQueryRunner();
        sql = "select * from t_book where is_deleted = ? and state=?";
        try {
            return queryRunner.query(sql, new ResultSetHandler<List<Book>>() {
                @Override
                public List<Book> handle(ResultSet rs) throws SQLException {
                    List<Book> list = new ArrayList<>();
                    while (rs.next()) {
                        Book b = new Book();
                        b.setId(rs.getInt("id"));
                        b.setBookName(rs.getString("book_name"));
                        b.setAuthor(rs.getString("author"));
                        b.setPublish(rs.getString("publish"));
                        b.setIsbn(rs.getString("isbn"));
                        String introduction = rs.getString("introduction");
                        if (introduction != null && !"".equals(introduction) && introduction.length() > 20) {
                            introduction = rs.getString("introduction").substring(0, 20);
                        }
                        b.setIntroduction(introduction + "...");
                        b.setLanguage(rs.getString("language"));
                        b.setPrice(rs.getBigDecimal("price"));
                        b.setPubdate(rs.getDate("pubdate"));
                        b.setPressmark(rs.getString("pressmark"));
                        b.setIsDeleted(rs.getInt("is_deleted"));
                        b.setState(rs.getInt("state"));
                        list.add(b);
                    }
                    return list;
                }
            }, DelFlagE.NO.code, code);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

}
