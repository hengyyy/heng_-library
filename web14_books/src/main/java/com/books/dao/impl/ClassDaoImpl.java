package com.books.dao.impl;

import com.books.bean.Class;
import com.books.dao.ClassDao;
import com.books.utils.DelFlagE;
import com.books.utils.MyDbutils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author xiaolong
 * @date 2022年10月19日 18:00
 */

public class ClassDaoImpl implements ClassDao {
    QueryRunner queryRunner;
    String sql;

    @Override
    public List<Class> list(Class cls) {
        queryRunner = MyDbutils.getQueryRunner();
        sql = "select * from t_class where is_deleted=?";
        try {
            return queryRunner.query(sql, new ResultSetHandler<List<Class>>() {
                @Override
                public List<Class> handle(ResultSet rs) throws SQLException {
                    List<Class> list = new ArrayList<>();
                    while (rs.next()) {
                        Class cls = new Class();
                        cls.setId(rs.getInt("id"));
                        cls.setClassName(rs.getString("class_name"));
                        cls.setClassDesc(rs.getString("class_desc"));
                        cls.setDeptId(rs.getInt("dept_id"));
                        cls.setDeptName(rs.getString("dept_name"));
                        cls.setIsDeleted(rs.getInt("is_deleted"));
                        list.add(cls);
                    }
                    return list;
                }
            }, DelFlagE.NO.code);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Integer saveClass(Class cls) {
        queryRunner = MyDbutils.getQueryRunner();
        sql = "insert into t_class(class_name,class_desc,dept_id,dept_name)values(?,?,?,?);";
        try {
            return queryRunner.update(sql, cls.getClassName(), cls.getClassDesc(),
                    cls.getDeptId(), cls.getDeptName());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }

//    public static void main(String[] args) {
//        ClassDaoImpl classDao = new ClassDaoImpl();
//        System.out.println(classDao.saveClass(new Class(100, "歪点子", "微电子", 26, "管理学院", 1)));
//    }

    @Override
    public Integer updateClass(Class cls) {
        queryRunner = MyDbutils.getQueryRunner();
        sql = "update t_class set class_name=?,class_desc=?,dept_id=?,dept_name=? where id=?";
        try {
            return queryRunner.update(sql, cls.getClassName(), cls.getClassDesc(), cls.getDeptId(), cls.getDeptName(), cls.getId());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }

    @Override
    public Integer deleteById(Integer id) {
        queryRunner = MyDbutils.getQueryRunner();
        sql = "update t_class set is_deleted=? where id=?";
        try {
            return queryRunner.update(sql, DelFlagE.YES.code, id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }

    @Override
    public Class queryById(Integer id) {
        queryRunner = MyDbutils.getQueryRunner();
        sql = "select * from t_class where is_deleted=? and id=?";
        try {
            return queryRunner.query(sql, new ResultSetHandler<Class>() {
                @Override
                public Class handle(ResultSet rs) throws SQLException {
                    if (rs.next()) {
                        Class cls = new Class();
                        cls.setId(rs.getInt("id"));
                        cls.setClassName(rs.getString("class_name"));
                        cls.setClassDesc(rs.getString("class_desc"));
                        cls.setDeptId(rs.getInt("dept_id"));
                        cls.setDeptName(rs.getString("dept_name"));
                        cls.setIsDeleted(rs.getInt("is_deleted"));
                        return cls;
                    }
                    return null;
                }
            }, DelFlagE.NO.code, id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<Class> queryByDepartId(Integer departId) {
        queryRunner = MyDbutils.getQueryRunner();
        sql = "select * from t_class where is_deleted=? and dept_id=?";
        try {
            return queryRunner.query(sql, new ResultSetHandler<List<Class>>() {
                @Override
                public List<Class> handle(ResultSet rs) throws SQLException {
                    List<Class> list = new ArrayList<>();
                    while (rs.next()) {
                        Class cls = new Class();
                        cls.setId(rs.getInt("id"));
                        cls.setClassName(rs.getString("class_name"));
                        cls.setClassDesc(rs.getString("class_desc"));
                        cls.setDeptId(rs.getInt("dept_id"));
                        cls.setDeptName(rs.getString("dept_name"));
                        cls.setIsDeleted(rs.getInt("is_deleted"));
                        list.add(cls);
                    }
                    return list;
                }
            }, DelFlagE.NO.code, departId);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}

