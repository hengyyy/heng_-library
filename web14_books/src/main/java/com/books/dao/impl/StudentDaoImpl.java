package com.books.dao.impl;

import com.books.bean.Student;
import com.books.dao.StudentDao;
import com.books.utils.DelFlagE;
import com.books.utils.MyDbutils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author xiaolong
 * @date 2022年10月19日 18:01
 */

public class StudentDaoImpl implements StudentDao {
    QueryRunner queryRunner = MyDbutils.getQueryRunner();
    String sql;

    @Override
    public List<Student> list(final Student stu) {

        sql = "select * from t_student where is_deleted=?";
        try {
            return queryRunner.query(sql, new ResultSetHandler<List<Student>>() {
                @Override
                public List<Student> handle(ResultSet rs) throws SQLException {
                    List<Student> list = new ArrayList<>();
                    while (rs.next()) {
                        Student student = new Student();
                        student.setId(rs.getInt("id"));
                        student.setStuNum(rs.getString("stu_num"));
                        student.setStuName(rs.getString("stu_name"));
                        student.setAddress(rs.getString("address"));
                        student.setPhoneNum(rs.getString("phone_num"));
                        student.setGender(rs.getString("gender"));
                        student.setDepartId(rs.getInt("departid"));
                        student.setDepartName(rs.getString("depart_name"));
                        student.setClassId(rs.getInt("classid"));
                        student.setClassName(rs.getString("class_name"));
                        student.setIsDeleted(rs.getInt("is_deleted"));
                        list.add(student);
                    }
                    return list;
                }
            }, DelFlagE.NO.code);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Integer saveStudent(Student stu) {
        sql = "insert into t_student(stu_num,stu_name,phone_num,gender,address,classid,class_name,departid,depart_name)values(?,?,?,?,?,?,?,?,?)";
        try {
            return queryRunner.update(sql, stu.getStuNum(), stu.getStuName(),
                    stu.getPhoneNum(), stu.getGender(),
                    stu.getAddress(), stu.getClassId(), stu.getClassName(),
                    stu.getDepartId(), stu.getDepartName());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }

    @Override
    public Integer updateStudent(Student stu) {
        sql = "update t_student set stu_num=?,stu_name=?,phone_num=?,gender=?,address=?,classid=?,class_name=?,departid=?,depart_name=? where id=?";
        try {
            return queryRunner.update(sql, stu.getStuNum(), stu.getStuName(),
                    stu.getPhoneNum(), stu.getGender(),
                    stu.getAddress(), stu.getClassId(), stu.getClassName(),
                    stu.getDepartId(), stu.getDepartName(), stu.getId());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }

    @Override
    public Integer deleteById(Integer id) {
        sql = "update t_student set is_deleted=? where id=?";
        try {
            return queryRunner.update(sql, DelFlagE.YES.code, id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }

    @Override
    public Student queryById(Integer id) {
        sql = "select * from t_student where is_deleted=? and id=?";
        try {
            return queryRunner.query(sql, new ResultSetHandler<Student>() {
                @Override
                public Student handle(ResultSet rs) throws SQLException {
                    if (rs.next()) {
                        Student student = new Student();
                        student.setId(rs.getInt("id"));
                        student.setStuNum(rs.getString("stu_num"));
                        student.setStuName(rs.getString("stu_name"));
                        student.setAddress(rs.getString("address"));
                        student.setPhoneNum(rs.getString("phone_num"));
                        student.setGender(rs.getString("gender"));
                        student.setDepartId(rs.getInt("departid"));
                        student.setDepartName(rs.getString("depart_name"));
                        student.setClassId(rs.getInt("classid"));
                        student.setClassName(rs.getString("class_name"));
                        student.setIsDeleted(rs.getInt("is_deleted"));
                        return student;
                    }
                    return null;
                }
            }, DelFlagE.NO.code, id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 统计班级下的学生个数
     *
     * @param classId
     * @return
     */

    @Override
    public int queryByClassIdCount(String classId) {
        sql = "select count(1) from t_student where is_deleted=? and classid=?";//count(1)表示记录数
        try {
            return queryRunner.query(sql, new ResultSetHandler<Integer>() {
                @Override
                public Integer handle(ResultSet rs) throws SQLException {
                    rs.next();
                    return rs.getInt(1);
                }
            }, DelFlagE.NO.code, classId);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public Student queryPhone(String phone) {
        sql = "select * from t_student where is_deleted=? and phone_num=?";
        try {
            return queryRunner.query(sql, new ResultSetHandler<Student>() {
                @Override
                public Student handle(ResultSet rs) throws SQLException {
                    if (rs.next()) {
                        Student student = new Student();
                        student.setId(rs.getInt("id"));
                        student.setStuNum(rs.getString("stu_num"));
                        student.setStuName(rs.getString("stu_name"));
                        student.setAddress(rs.getString("address"));
                        student.setPhoneNum(rs.getString("phone_num"));
                        student.setGender(rs.getString("gender"));
                        student.setDepartId(rs.getInt("departid"));
                        student.setDepartName(rs.getString("depart_name"));
                        student.setClassId(rs.getInt("classid"));
                        student.setClassName(rs.getString("class_name"));
                        student.setIsDeleted(rs.getInt("is_deleted"));
                        return student;
                    }
                    return null;
                }
            }, DelFlagE.NO.code, phone);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
