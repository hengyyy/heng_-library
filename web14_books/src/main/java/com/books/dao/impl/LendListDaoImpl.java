package com.books.dao.impl;

import com.books.bean.LendList;
import com.books.dao.LendListDao;
import com.books.utils.MyDbutils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

/**
 * @author xiaolong
 * @date 2022年10月28日 9:49
 */

public class LendListDaoImpl implements LendListDao {
    /**
     * 借阅
     *
     * @param lendList
     * @return
     */
    @Override
    public Integer borrowLendList(LendList lendList) {
        QueryRunner queryRunner = MyDbutils.getQueryRunner();
        String sql = "insert into t_lend_list(book_id,card_id,lend_date)values(?,?,?)";
        try {
            return queryRunner.update(sql, lendList.getBookId(), lendList.getCardId(), lendList.getLendDate());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }

    /**
     * 归还
     *
     * @param lendList
     * @return
     */
    @Override
    public Integer backLendList(LendList lendList) {
        QueryRunner queryRunner = MyDbutils.getQueryRunner();
        String sql = "update t_lend_list set back_date=? where id=? ";
        try {
            return queryRunner.update(sql, lendList.getBackDate(), lendList.getId());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }

    @Override
    public LendList queryByCardId(final String cardId) {
        QueryRunner queryRunner = MyDbutils.getQueryRunner();
        String sql = "select * from t_lend_list where card_id=?";
        try {
            return queryRunner.query(sql, new ResultSetHandler<LendList>() {
                @Override
                public LendList handle(ResultSet rs) throws SQLException {
                    if (rs.next()) {
                        LendList lendList = new LendList();
                        lendList.setId(rs.getInt("id"));
                        lendList.setCardId(cardId);
                        lendList.setBookId(rs.getInt("book_id"));
                        lendList.setBackDate(new Date());
                        lendList.setLendDate(rs.getDate("lend_date"));
                        return lendList;
                    }
                    return null;
                }
            }, cardId);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
