package com.books.dao.impl;

import com.books.bean.Department;
import com.books.dao.DepartmentDao;
import com.books.utils.DelFlagE;
import com.books.utils.MyDbutils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author xiaolong
 * @date 2022年10月19日 17:59
 */

public class DepartmentDaoImpl implements DepartmentDao {
    QueryRunner queryRunner;
    String sql;

    @Override
    public List<Department> list(Department dept) {
        queryRunner = MyDbutils.getQueryRunner();
        sql = "select *from t_department where is_deleted=?";
        try {
            return queryRunner.query(sql, new ResultSetHandler<List<Department>>() {

                @Override
                public List<Department> handle(ResultSet rs) throws SQLException {
                    List<Department> list = new ArrayList<>();
                    while (rs.next()) {
                        Department dept = new Department();
                        dept.setId(rs.getInt("id"));
                        dept.setDepartment(rs.getString("department"));
                        dept.setDeptDesc(rs.getString("dept_desc"));
                        dept.setIsDeleted(rs.getInt("is_deleted"));
                        list.add(dept);
                    }
                    return list;
                }
            }, DelFlagE.NO.code);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Integer saveDepartment(Department dept) {
        queryRunner = MyDbutils.getQueryRunner();
        sql = "insert into t_department(department,dept_desc)values(?,?)";
        try {
            return queryRunner.update(sql, dept.getDepartment(), dept.getDeptDesc());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }

    @Override
    public Integer updateDepartment(Department dept) {
        queryRunner = MyDbutils.getQueryRunner();
        sql = "update t_department set department=?,dept_desc=? where id=? ;";
        //update t_department set department=?, dept_desc=? where id=?;
        try {
            return queryRunner.update(sql, dept.getDepartment(), dept.getDeptDesc(), dept.getId());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }

    @Override
    public Integer deleteById(Integer id) {
        queryRunner = MyDbutils.getQueryRunner();
        sql = "update t_department set is_deleted = ? where id = ?";
        try {
            return queryRunner.update(sql, DelFlagE.YES.code, id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }

    @Override
    public Department queryById(Integer id) {
        queryRunner = MyDbutils.getQueryRunner();
        sql = "select * from t_department where is_deleted=? and id=?";
        try {
            return queryRunner.query(sql, new ResultSetHandler<Department>() {

                @Override
                public Department handle(ResultSet rs) throws SQLException {
                    if (rs.next()) {
                        Department dept = new Department();
                        dept.setId(rs.getInt("id"));
                        dept.setDepartment(rs.getString("department"));
                        dept.setDeptDesc(rs.getString("dept_desc"));
                        dept.setIsDeleted(rs.getInt("is_deleted"));
                        return dept;
                    }
                    return null;
                }
            }, DelFlagE.NO.code, id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
