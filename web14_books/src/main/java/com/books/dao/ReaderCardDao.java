package com.books.dao;

import com.books.bean.ReaderCard;

import java.util.List;

/**
 * 借书卡的Dao接口
 */
public interface ReaderCardDao {
    public List<ReaderCard> list(ReaderCard card);

    public Integer saveReaderCard(ReaderCard card);

    public Integer updateReaderCard(ReaderCard card);

    public Integer deleteById(Integer id);

    public ReaderCard queryById(Integer id);


    List<ReaderCard> queryByStuId(Integer id);

}
