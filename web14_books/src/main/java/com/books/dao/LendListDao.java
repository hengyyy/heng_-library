package com.books.dao;

import com.books.bean.LendList;

public interface LendListDao {

    public Integer borrowLendList(LendList lendList);

    public Integer backLendList(LendList lendList);

    LendList queryByCardId(String cardId);
}
