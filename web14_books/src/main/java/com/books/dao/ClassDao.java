package com.books.dao;

import com.books.bean.Class;

import java.util.List;

public interface ClassDao {
    public List<Class> list(Class cls);

    public Integer saveClass(Class cls);

    public Integer updateClass(Class cls);

    public Integer deleteById(Integer id);

    public Class queryById(Integer id);

    public List<Class> queryByDepartId(Integer departId);
}
