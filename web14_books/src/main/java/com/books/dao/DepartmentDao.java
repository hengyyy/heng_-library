package com.books.dao;

import com.books.bean.Department;

import java.util.List;

public interface DepartmentDao {

    public List<Department> list(Department dept);

    public Integer saveDepartment(Department dept);

    public Integer updateDepartment(Department dept);

    public Integer deleteById(Integer id);

    public Department queryById(Integer id);
}
