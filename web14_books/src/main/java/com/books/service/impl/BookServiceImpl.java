package com.books.service.impl;

import com.books.bean.Book;
import com.books.dao.BookDao;
import com.books.dao.impl.BookDaoImpl;
import com.books.service.BookService;

import java.util.List;

/**
 * @author xiaolong
 * @date 2022年10月18日 21:04
 */

public class BookServiceImpl implements BookService {
    private BookDao bookDao = new BookDaoImpl();

    @Override
    public List<Book> list(Book book) {
        return bookDao.list(book);
    }

    @Override
    public Integer saveBook(Book book) {
        return bookDao.saveBook(book);
    }

    @Override
    public Integer updateBook(Book book) {
        return bookDao.updateBook(book);
    }

    @Override
    public Integer deleteById(Integer id) {
        return bookDao.deleteById(id);
    }

    @Override
    public Book queryById(Integer id) {
        return bookDao.queryById(id);
    }

    @Override
    public List<Book> queryListByState(int code) {
        return bookDao.queryListByState(code);
    }
}
