package com.books.service.impl;

import com.books.bean.Book;
import com.books.bean.LendList;
import com.books.bean.ReaderCard;
import com.books.dao.LendListDao;
import com.books.dao.impl.LendListDaoImpl;
import com.books.service.BookService;
import com.books.service.LendListService;
import com.books.service.ReaderCardService;
import com.books.utils.BookFlagE;

/**
 * @author xiaolong
 * @date 2022年10月28日 9:47
 */

public class LendListServiceImpl implements LendListService {
    LendListDao lendListDao = new LendListDaoImpl();
    ReaderCardService readerCardService = new ReaderCardServiceImpl();
    BookService bookService = new BookServiceImpl();

    @Override
    public Integer borrowLendList(LendList lendList) {
        //更新借书卡的状态和书籍的状态
        ReaderCard readerCard = readerCardService.queryById(Integer.parseInt(lendList.getCardId()));
        readerCard.setState(1);
        readerCardService.updateReaderCard(readerCard);
        Book book = bookService.queryById(lendList.getBookId());
        book.setState(BookFlagE.BORROW.code);
        bookService.updateBook(book);
        return lendListDao.borrowLendList(lendList);
    }

    @Override
    public Integer backLendList(LendList lendList) {
        //更新借书卡的状态和书籍的状态
        ReaderCard readerCard = readerCardService.queryById(Integer.parseInt(lendList.getCardId()));
        readerCard.setState(0);
        readerCardService.updateReaderCard(readerCard);
        Book book = bookService.queryById(lendList.getBookId());
        book.setState(BookFlagE.FREE.code);
        bookService.updateBook(book);
        return lendListDao.backLendList(lendList);
    }

    @Override
    public LendList queryByCardId(String cardId) {
        return lendListDao.queryByCardId(cardId);
    }
}
