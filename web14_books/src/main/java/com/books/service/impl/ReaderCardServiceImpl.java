package com.books.service.impl;

import com.books.bean.ReaderCard;
import com.books.dao.ReaderCardDao;
import com.books.dao.impl.ReaderCardDaoImpl;
import com.books.service.ReaderCardService;

import java.util.List;

/**
 * @author xiaolong
 * @date 2022年10月27日 15:27
 */

public class ReaderCardServiceImpl implements ReaderCardService {
    ReaderCardDao readerCardDao = new ReaderCardDaoImpl();

    @Override
    public List<ReaderCard> list(ReaderCard card) {
        return readerCardDao.list(card);
    }

    @Override
    public Integer saveReaderCard(ReaderCard card) {

        return readerCardDao.saveReaderCard(card);
    }

    @Override
    public Integer updateReaderCard(ReaderCard card) {

        return readerCardDao.updateReaderCard(card);
    }

    @Override
    public Integer deleteById(Integer id) {
        return readerCardDao.deleteById(id);
    }

    @Override
    public ReaderCard queryById(Integer id) {
        return readerCardDao.queryById(id);
    }

    @Override
    public List<ReaderCard> queryByStuId(Integer id) {
        return readerCardDao.queryByStuId(id);
    }
}
