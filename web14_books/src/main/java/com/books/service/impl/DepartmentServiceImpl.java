package com.books.service.impl;

import com.books.bean.Department;
import com.books.dao.DepartmentDao;
import com.books.dao.impl.DepartmentDaoImpl;
import com.books.service.DepartmentService;

import java.util.List;

/**
 * @author xiaolong
 * @date 2022年10月19日 20:44
 */

public class DepartmentServiceImpl implements DepartmentService {
    DepartmentDao departmentDao = new DepartmentDaoImpl();

    @Override
    public List<Department> list(Department dept) {
        return departmentDao.list(dept);
    }

    @Override
    public Integer saveDepartment(Department dept) {
        return departmentDao.saveDepartment(dept);
    }

    @Override
    public Integer updateDepartment(Department dept) {
        return departmentDao.updateDepartment(dept);
    }

    @Override
    public Integer deleteById(Integer id) {
        return departmentDao.deleteById(id);
    }

    @Override
    public Department queryById(Integer id) {
        return departmentDao.queryById(id);
    }
}
