package com.books.service.impl;

import com.books.bean.User;
import com.books.dao.UserDao;
import com.books.dao.impl.UserDaoImpl;
import com.books.service.UserService;

import java.util.List;

/**
 * @author xiaolong
 * @date 2022年10月11日 16:30
 * 用户service接口的实现类
 * 通过Dao的调用来完成复杂的业务逻辑的处理
 */

public class UserServiceImpl implements UserService {
    private UserDao userDao = new UserDaoImpl();

    @Override
    public List<User> getUser(User user) {
        //处理对应的业务逻辑
        return userDao.list(user);
    }

    @Override
    public Integer addUser(User user) {
        return userDao.save(user);
    }

    @Override
    public Integer deleteById(Integer id) {

        return userDao.deleteById(id);

    }

    @Override
    public User queryById(Integer id) {
        return userDao.queryById(id);
    }

    @Override
    public Integer updateUser(User user) {
        return userDao.updateUser(user);
    }

    @Override
    public String checkUserName(String userName) {
        return userDao.checkUserName(userName);
    }

    @Override
    public User checkUserNameAndPassword(String userName, String password) {
        return userDao.checkUserNameAndPassword(userName, password);
    }


}
