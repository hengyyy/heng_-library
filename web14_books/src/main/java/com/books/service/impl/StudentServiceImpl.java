package com.books.service.impl;

import com.books.bean.Student;
import com.books.bean.User;
import com.books.dao.StudentDao;
import com.books.dao.impl.StudentDaoImpl;
import com.books.service.StudentService;
import com.books.service.UserService;

import java.util.List;

/**
 * @author xiaolong
 * @date 2022年10月19日 20:46
 */

public class StudentServiceImpl implements StudentService {
    StudentDao studentDao = new StudentDaoImpl();
    UserService userService = new UserServiceImpl();

    @Override
    public List<Student> list(Student stu) {
        return studentDao.list(stu);
    }

    @Override
    public Integer saveStudent(Student stu) {
        //当我们创建一个学生信息的时候 同步创建该学生的账号信息 学生姓名 手机号 密码是默认123456
        User user = new User();
        user.setPassword("123456");
        user.setUserName(stu.getPhoneNum());
        user.setPhoneNum(stu.getPhoneNum());
        userService.addUser(user);//创建账号
        return studentDao.saveStudent(stu);//保存学生信息
    }

    @Override
    public Integer updateStudent(Student stu) {
        return studentDao.updateStudent(stu);
    }

    @Override
    public Integer deleteById(Integer id) {
        return studentDao.deleteById(id);
    }

    @Override
    public Student queryById(Integer id) {
        return studentDao.queryById(id);
    }


    @Override
    public int queryByClassIdCount(String id) {
        return studentDao.queryByClassIdCount(id);
    }

    @Override
    public Student queryByPhone(String phone) {
        return studentDao.queryPhone(phone);
    }
}
