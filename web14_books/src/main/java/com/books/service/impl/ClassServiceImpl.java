package com.books.service.impl;

import com.books.bean.Class;
import com.books.dao.ClassDao;
import com.books.dao.impl.ClassDaoImpl;
import com.books.service.ClassService;

import java.util.List;

/**
 * @author xiaolong
 * @date 2022年10月19日 20:46
 */

public class ClassServiceImpl implements ClassService {
    ClassDao classDao = new ClassDaoImpl();

    @Override
    public List<Class> list(Class cls) {
        return classDao.list(cls);
    }

    @Override
    public Integer saveClass(Class cls) {
        return classDao.saveClass(cls);
    }

    @Override
    public Integer updateClass(Class cls) {
        return classDao.updateClass(cls);
    }

    @Override
    public Integer deleteById(Integer id) {
        return classDao.deleteById(id);
    }

    @Override
    public Class queryById(Integer id) {
        return classDao.queryById(id);
    }

    @Override
    public List<Class> queryByDepartId(Integer departId) {
        return classDao.queryByDepartId(departId);
    }
}
