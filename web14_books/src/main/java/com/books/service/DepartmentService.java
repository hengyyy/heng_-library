package com.books.service;

import com.books.bean.Department;

import java.util.List;

public interface DepartmentService {

    public List<Department> list(Department dept);

    public Integer saveDepartment(Department dept);

    public Integer updateDepartment(Department dept);

    public Integer deleteById(Integer id);

    public Department queryById(Integer id);
}
