package com.books.service;


import com.books.bean.User;

import java.util.List;

/**
 * @author xiaolong
 * @date 2022年10月11日 16:27
 * 用户的service接口
 */

public interface UserService {
    public List<User> getUser(User user);

    public Integer addUser(User user);

    public Integer deleteById(Integer id);

    public User queryById(Integer id);

    public Integer updateUser(User user);

    public String checkUserName(String userName);

    public User checkUserNameAndPassword(String userName, String password);

}
