package com.books.service;

import com.books.bean.LendList;

public interface LendListService {

    public Integer borrowLendList(LendList lendList);

    public Integer backLendList(LendList lendList);

    LendList queryByCardId(String cardId);
}
