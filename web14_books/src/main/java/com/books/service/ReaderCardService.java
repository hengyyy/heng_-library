package com.books.service;

import com.books.bean.ReaderCard;

import java.util.List;

/**
 * 逻辑层
 */
public interface ReaderCardService {
    public List<ReaderCard> list(ReaderCard card);

    public Integer saveReaderCard(ReaderCard card);

    public Integer updateReaderCard(ReaderCard card);

    public Integer deleteById(Integer id);

    public ReaderCard queryById(Integer id);

    List<ReaderCard> queryByStuId(Integer id);
}
