package com.books.bean;

/**
 * @author xiaolong
 * @date 2022年10月19日 16:35
 * 班级实体
 */

public class Class {
    private Integer id;
    private String className;
    private String classDesc;
    private Integer deptId;
    private String deptName;
    private Integer isDeleted;//

    public Class(Integer id, String className, String classDesc, Integer deptId, String deptName, Integer isDeleted) {
        this.id = id;
        this.className = className;
        this.classDesc = classDesc;
        this.deptId = deptId;
        this.deptName = deptName;
        this.isDeleted = isDeleted;
    }

    public Class(){}

    public Integer getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Integer isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getClassDesc() {
        return classDesc;
    }

    public void setClassDesc(String classDesc) {
        this.classDesc = classDesc;
    }

    public Integer getDeptId() {
        return deptId;
    }

    public void setDeptId(Integer deptId) {
        this.deptId = deptId;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    @Override
    public String toString() {
        return "Class{" +
                "id=" + id +
                ", className='" + className + '\'' +
                ", classDesc='" + classDesc + '\'' +
                ", deptId=" + deptId +
                ", deptName='" + deptName + '\'' +
                ", isDeleted=" + isDeleted +
                '}';
    }
}
