package com.books.utils;


import com.mysql.cj.jdbc.MysqlDataSource;
import org.apache.commons.dbutils.QueryRunner;

/**
 * @author xiaolong
 * @date 2022年10月11日 15:49
 * 数据库的工具类
 */

public class MyDbutils {
    private static MysqlDataSource dataSource;

    static {
        dataSource = new MysqlDataSource();
        dataSource.setUser("root");
        dataSource.setPassword("ZHa123456789.");
        dataSource.setUrl("jdbc:mysql://127.0.0.1:3306/csdncourse?useUnicode=true&characterEncoding=UTF8&serverTimezone=Asia/Shanghai&useSSL=false");
    }

    public static QueryRunner getQueryRunner() {
        return new QueryRunner(dataSource);

    }
}
