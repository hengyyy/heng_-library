package com.books.utils;

/**
 * @author xiaolong
 * @date 2022年10月19日 21:03
 * 操作字符串的工具类
 */

public class StringUtils {
    /**
     * 判断字符串是否为空
     */
    public static boolean isEmpty(String msg) {
        return msg == null || "".equals(msg);
    }

    /**
     * 判断字符串是否不为空
     */
    public static boolean isNotEmpty(String msg) {
        return !(msg == null || "".equals(msg));
    }
}
