package com.books.servlet;

import com.books.bean.Class;
import com.books.bean.Department;
import com.books.service.ClassService;
import com.books.service.DepartmentService;
import com.books.service.impl.ClassServiceImpl;
import com.books.service.impl.DepartmentServiceImpl;
import com.books.utils.Constant;
import com.books.utils.RequestParameterUtils;
import com.books.utils.StringUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

/**
 * @author xiaolong
 * @date 2022年10月20日 10:16
 */
@WebServlet("/departmentServlet")
public class DepartmentServlet extends HttpServlet {
    DepartmentService departmentService = new DepartmentServiceImpl();
    ClassService classService = new ClassServiceImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //1.获取请求的类型
        String type = req.getParameter(Constant.REQUEST_PARAMETER_TYPE);
        //2.如果type不为空 那么根据type的不同值做出不同的操作
        if (StringUtils.isNotEmpty(type)) {
            if (Constant.SERVLET_TYPE_QUERY.equals(type)) {
                queryList(req, resp);
            } else if (Constant.SERVLET_TYPE_SAVE.equals(type)) {
                //保存院系信息
                saveOrUpdateDept(req, resp);
            } else if (Constant.SERVLET_TYPE_UPDATE.equals(type)) {
                saveOrUpdateDept(req, resp);
            } else if (Constant.SERVLET_TYPE_DELETE.equals(type)) {
                deleteDepartment(req, resp);
            } else if (Constant.SERVLET_TYPE_QUERYBYID.equals(type)) {
                //根据id查询院系信息
                String id = req.getParameter("id");
                Department department = departmentService.queryById(Integer.parseInt(id));
                //把数据保存在Request作用域中
                req.setAttribute("dept", department);
                req.getRequestDispatcher("/depart/deptUpdate.jsp").forward(req, resp);
            } else {
                queryList(req, resp);
            }
        } else {
            queryList(req, resp);
        }
    }

    private void saveOrUpdateDept(HttpServletRequest req, HttpServletResponse resp) {
        //保存院系信息
        try {
            Department dept = RequestParameterUtils.getRequestParameterForReflect(req, Department.class);
            if (dept.getId() != null && dept.getId() > 0) {
                //更新
                departmentService.updateDepartment(dept);
            } else {
                //添加
                departmentService.saveDepartment(dept);
            }
            // 重定向做查询操作
            resp.sendRedirect(req.getContextPath() + "/departmentServlet?type=query");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void deleteDepartment(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        //根据id删除院系信息 --逻辑删除
        String id = req.getParameter("id");
        //删除院系信息 前我们需要判断是否有班级信息 如果有就不让删除
        List<Class> classes = classService.queryByDepartId(Integer.parseInt(id));
        if (classes != null && classes.size() > 0) {
            //说明该院系下有班级就不能删除
            resp.setContentType("text/html;charset=UTF-8");
            PrintWriter writer = resp.getWriter();
            writer.println("<script type='text/javascript'>");
            writer.println("alert('当前院系下有班级,不能删除!!!')");
            writer.println("location.href='/web14_books/departmentServlet?type=query';");
            writer.println("</script>");
            writer.flush();
        } else {
            departmentService.deleteById(Integer.parseInt(id));
            resp.sendRedirect(req.getContextPath() + "/departmentServlet?type=query");
        }

    }

    private void queryList(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //查询出所有放入院系信息
        List<Department> list = departmentService.list(null);
        //存储到Request作用域中
        req.setAttribute("list", list);
        //然后跳转到院系的展示界面
        req.getRequestDispatcher("/depart/depart.jsp").forward(req, resp);
    }
}


