package com.books.servlet;


import com.books.bean.User;
import com.books.service.UserService;
import com.books.service.impl.UserServiceImpl;
import com.books.utils.Constant;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * @author xiaolong
 * @date 2022年10月17日 19:54
 * 处理登录和注销的servlet
 */
@WebServlet("/loginServlet")
public class LoginServlet extends HttpServlet {
    UserService userService = new UserServiceImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //实现登录的功能
        //1.获取表单提交的账号密码
        String userName = req.getParameter("userName");
        String password = req.getParameter("password");
        //2.调用Service的相关方法来实现处理
        User user = userService.checkUserNameAndPassword(userName, password);
        HttpSession session = req.getSession();
        //3.根据验证结果来做出对应的响应
        if (user != null) {
            //我们不能把密码存储到Session
            //登录成功 把登录成功的用户信息存储到Session中
            session.setAttribute(Constant.SESSION_LOGIN_USER, user);
            //登录成功
            resp.sendRedirect(req.getContextPath() + "/main.jsp");
        } else {
            //登录失败
            session.setAttribute("msg", "用户名或密码错误");
            resp.sendRedirect(req.getContextPath() + "/login.jsp");
        }
    }
}
