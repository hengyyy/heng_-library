package com.books.servlet;

import com.books.bean.Book;
import com.books.bean.ReaderCard;
import com.books.bean.Student;
import com.books.bean.User;
import com.books.service.BookService;
import com.books.service.ReaderCardService;
import com.books.service.StudentService;
import com.books.service.impl.BookServiceImpl;
import com.books.service.impl.ReaderCardServiceImpl;
import com.books.service.impl.StudentServiceImpl;
import com.books.utils.BookFlagE;
import com.books.utils.Constant;
import com.books.utils.RequestParameterUtils;
import com.books.utils.StringUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

/**
 * @author xiaolong
 * @date 2022年10月27日 15:31
 */
@WebServlet("/readerCardServlet")
public class ReaderCardServlet extends HttpServlet {
    ReaderCardService readerCardService = new ReaderCardServiceImpl();
    StudentService studentService = new StudentServiceImpl();
    BookService bookService = new BookServiceImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        //1.获取请求的类型
        String type = req.getParameter(Constant.REQUEST_PARAMETER_TYPE);
        //2.如果type不为空，那么根据type的不同值做出不同的操作
        if (StringUtils.isNotEmpty(type)) {
            if (Constant.SERVLET_TYPE_QUERY.equals(type)) {
                queryList(req, resp);
            } else if (Constant.SERVLET_TYPE_SAVE.equals(type)) {
                saveCard(req, resp);
            } else if (Constant.SERVLET_TYPE_UPDATE.equals(type)) {
            } else if (Constant.SERVLET_TYPE_DELETE.equals(type)) {
            } else if (Constant.SERVLET_TYPE_QUERYBYID.equals(type)) {

            } else if ("saveBefor".equals(type)) {

                //查询出所有放入学生信息
                List<Student> list = studentService.list(null);
                req.setAttribute("stus", list);
                req.getRequestDispatcher("/card/cardAdd.jsp").forward(req, resp);

            } else if ("queryBooks".equals(type)) {
                //获取借书卡的编号
                String cardId = req.getParameter("id");
                //查询所有可以借阅的书籍
                List<Book> list = bookService.queryListByState(BookFlagE.FREE.code);
                req.setAttribute("books", list);
                req.setAttribute("cardId", cardId);
                req.getRequestDispatcher("/card/bookBorrow.jsp").forward(req, resp);
            } else {
            }
        } else {

        }

    }

    private void saveCard(HttpServletRequest req, HttpServletResponse resp) {
        //生成借书卡
        try {
            ReaderCard readerCard = RequestParameterUtils.getRequestParameterForReflect(req, ReaderCard.class);
            //根据学生编号 获取对应的学生姓名
            Student student = studentService.queryById(readerCard.getStuId());
            readerCard.setStuName(student.getStuName());
            //生成借书卡的密码

            String password = UUID.randomUUID().toString();
            readerCard.setPassword(password);
            readerCardService.saveReaderCard(readerCard);
            resp.sendRedirect(req.getContextPath() + "/readerCardServlet?type=query");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void queryList(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //查询所有的借书卡信息
        //查询当前登录用户对应的学生的学生卡的信息
        User user = (User) req.getSession().getAttribute(Constant.SESSION_LOGIN_USER);
        List<ReaderCard> list = null;
        if (!"admin".equals(user.getUserName())) {
            //如果不是管理员 那么就不能查询所有的数据
            //我们需要根据当前登录的学生用户找到对应的学生信息
            String phone = user.getUserName();
            Student stu = studentService.queryByPhone(phone);
            //根据学生的编号查询 借书卡的情况
            if (stu != null) {
                list = readerCardService.queryByStuId(stu.getId());
            }
        } else {
            //
            list = readerCardService.list(null);
        }

        req.setAttribute("list", list);
        req.getRequestDispatcher("/card/card.jsp").forward(req, resp);
    }
}


