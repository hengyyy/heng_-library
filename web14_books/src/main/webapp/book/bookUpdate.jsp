<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2022/10/12
  Time: 19:50
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>无标题文档</title>
    <link href="/css/style.css" rel="stylesheet" type="text/css"/>
    <link href="/css/select.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="/js/jquery.js"></script>
    <script type="text/javascript" src="/js/jquery.idTabs.min.js"></script>
    <script type="text/javascript" src="/js/select-ui.min.js"></script>
    <script type="text/javascript" src="/editor/kindeditor.js"></script>
    <script type="text/javascript">
        $(document).ready(function (e) {
            $(".select1").uedSelect({
                width: 345
            });
        });
    </script>
</head>

<body>

<div class="place">
    <span>位置：</span>
    <ul class="placeul">
        <li><a href="#">首页</a></li>
        <li><a href="#">表单</a></li>
    </ul>
</div>
<div class="formbody">

    <div class="formtitle"><span>书籍信息</span></div>
    <%--${pageContext.request.contextPath}是JSP取得绝对路径的方法--%>
    <form action="${pageContext.request.contextPath}/bookServlet" method="post" id="myForm">
        <ul class="forminfo">
    <%--<c:if test=""></c:if> jsp标准标签库 如果表达式里的判断为true 就会输出标签体中的内容--%>
            <c:if test="${empty book}">
                <input type="hidden" name="type" value="save">
            </c:if>
            <c:if test="${not empty book}">
                <input type="hidden" name="type" value="update">
                <input type="hidden" name="id" value="${book.id}">
            </c:if>
            <%--requestScope相当于作用域 一般是从服务器传递结果到页面，在页面中取出服务器端保存的值！--%>
            <%--一般是从服务器传递结果到页面，在页面中取出服务器端保存的值！--%>
            <li>
                <label>书名<b>*</b></label>
                <input name="bookName" id="bookName" type="text" class="dfinput" value="${requestScope.book.bookName}"/>
                <i id="userNameI">标题不能超过30个字符</i>
            </li>

            <li>
                <label>作者<b>*</b></label>
                <input name="author" type="text" class="dfinput" value="${requestScope.book.author}"/>
                <i>多个关键字用,隔开</i>
            </li>

            <li>
                <label>出版社<b>*</b></label>
                <input name="publish" type="text" class="dfinput" value="${requestScope.book.publish}"/>
                <i>多个关键字用,隔开</i>
            </li>

            <li>
                <label>书籍编码<b>*</b></label>
                <input name="isbn" type="text" class="dfinput" value="${requestScope.book.isbn}"/>
                <i>多个关键字用,隔开</i>
            </li>

            <li>
                <label>书籍语言<b>*</b></label>
                <div class="vocation">
                    <select class="select1" name="language">
                        <option value="${requestScope.book.language=='中文'?'selected':''}">中文</option>
                        <option value="${requestScope.book.language=='英语'?'selected':''}">英语</option>
                        <option value="${requestScope.book.language=='日文'?'selected':''}">日文</option>
                        <option value="${requestScope.book.language=='韩语'?'selected':''}">韩语</option>
                    </select>
                </div>
            </li>

            <li>
                <label>书籍价格<b>*</b></label>
                <input name="price" type="text" class="dfinput" value="${requestScope.book.price}"/>
                <i>多个关键字用,隔开</i>
            </li>

            <li>
                <label>发布日期<b>*</b></label>
                <input name="pubdate" type="date" class="dfinput" value="${requestScope.book.pubdate}"/>
                <i>多个关键字用,隔开</i>
            </li>

            <li>
                <label>书架号<b>*</b></label>
                <input name="pressmark" type="text" class="dfinput" value="${requestScope.book.pressmark}"/>
                <i>多个关键字用,隔开</i>
            </li>

            <li>
                <label>书籍介绍<b>*</b></label>
                <textarea name="introduction" cols="" rows="" class="textinput">
                    ${requestScope.book.introduction}
                </textarea>
            </li>


            <li>
                <label>&nbsp;</label>
                <input type="button" id="btn" class="btn" value="确认保存"/>
            </li>
        </ul>
    </form>

</div>
<script type="application/javascript">
    var flag = true;
    $(function () {
        $("#userName").blur(function () {
        });
        $("#btn").click(function () {
            $("#myForm").submit();
        })
    })
</script>

<div style="display:none">
    <script src='http://v7.cnzz.com/stat.php?id=155540&web_id=155540' language='JavaScript' charset='gb2312'></script>
</div>
</body>
</html>
